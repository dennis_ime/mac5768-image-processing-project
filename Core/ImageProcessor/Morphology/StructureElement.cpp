#include "StructureElement.h"

namespace ConnectedOperatorApp
{
    Point::Point(int p_x, int p_y): x(p_x), y(p_y)
    {}

    std::vector<Point> StructureElement::getPoints()
    {
        return points;
    }

    std::set<int> StructureElement::allLines()
    {
        std::set<int> lines;

        for(auto point : points)
            lines.insert(point.y);

        return lines;
    }

    StructureElement* StructureElement::reflexion()
    {
        StructureElement* structureElement = new StructureElement();

        for (auto p : points)
            structureElement->points.push_back(Point(-p.x, -p.y));

        return structureElement;
    }

    BoxStructureElement::BoxStructureElement(unsigned int p_width, unsigned int p_height)
        :width(p_width), height(p_height)
    {
        int halfWidth = divideIndexInBorders(width, 2);
        int halfHeight = divideIndexInBorders(height, 2);

        for (int i = -halfWidth; i <= halfWidth; ++i)
            for (int j = -halfHeight; j <= halfHeight; ++j)
                points.push_back(Point(i, j));
    }

    unsigned int BoxStructureElement::getWidth() { return width; }
    unsigned int BoxStructureElement::getHeight() { return height; }

    int BoxStructureElement::divideIndexInBorders(int x, int y)
    {
        if (x % 2 == 0)
            return x / y - 1;

        return x / y;
    }

    CircleStructureElement::CircleStructureElement(unsigned int p_radius)
        :radius(p_radius)
    {
        //TODO: I Must Code it!
    }

    unsigned int CircleStructureElement::getRadius() { return radius; }
}
