#ifndef STRUCTUREELEMENT_H_INCLUDED
#define STRUCTUREELEMENT_H_INCLUDED

#include <vector>
#include <set>

namespace ConnectedOperatorApp
{
    class Point
    {
        public:
            int x, y;
            Point(int p_x, int p_y);
    };

    class StructureElement
    {
        protected:
            std::vector<Point> points;

        public:
            std::vector<Point> getPoints();
            std::set<int> allLines();

            StructureElement* reflexion();
    };

    class BoxStructureElement : public StructureElement
    {
        private:
            unsigned int width;
            unsigned int height;

        public:
            BoxStructureElement(unsigned int p_width, unsigned int p_height);

            unsigned int getWidth();
            unsigned int getHeight();

        private:
            int divideIndexInBorders(int x, int y);

    };

    class CircleStructureElement : public StructureElement
    {
        private:
            unsigned int radius;

        public:
            CircleStructureElement(unsigned int p_radius);

            unsigned int getRadius();
    };
}
#endif // STRUCTUREELEMENT_H_INCLUDED
