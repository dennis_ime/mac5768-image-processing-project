#include "MorphologyOperators.h"
#include<iostream>
#include<cstdio>

namespace ConnectedOperatorApp
{
    std::map<int, BYTE*> MorphologyOperator::getLinesFromStructureElement(ImageWrapper& image, int currentLine)
    {
        std::map<int, BYTE*> lines;
        std::set<int> linesIndex = structureElement->allLines();
        unsigned int height = image.getHeight();

        for (auto line : linesIndex)
        {
            int lineMoved = line + currentLine;
            if(lineMoved < 0 || image.getHeight() <= height)
                lines[line] = image.getScanLine(lineMoved);
        }

        return lines;
    }


    DilationMorphology::DilationMorphology(StructureElement* p_structureElement)
    {
        structureElement = p_structureElement->reflexion();
        delete p_structureElement;
    }


    ImageWrapper MorphologyOperator::process(ImageWrapper& image)
    {
        ImageWrapper result = image.createAnEmptyCopy();
        unsigned int height = image.getHeight();
        unsigned int width = image.getWidth();

        for (unsigned int i = 0; i < height; ++i)
        {
            BYTE* currentLine = result.getScanLine(i);
            std::map<int, BYTE*> linesFromStructureElement = getLinesFromStructureElement(image, i);

            for (unsigned int j=0; j < width; ++j)
            {
                Point currentPoint(j, i);
                currentLine[j] = getCurrentElementFromOperator(image, currentPoint, linesFromStructureElement);
            }
        }

        return result;
    }

    BYTE DilationMorphology::getCurrentElementFromOperator(ImageWrapper& image, Point& currentPixels, std::map<int, BYTE*> linesStructureElement)
    {
        std::vector<Point> points = structureElement->getPoints();
        BYTE result = 0;

        unsigned int width = image.getWidth();
        unsigned int height = image.getHeight();

        for (unsigned int i = 0; i < points.size(); ++i)
        {
            int column = currentPixels.x + points[i].x;
            int row = currentPixels.y + points[i].y;

            if(column > 0 && column < (int)width && row > 0 && row < (int)height)
            {
                BYTE* line = linesStructureElement[points[i].y];

                if(result < line[column])
                    result = line[column];
            }
            else
                result = 255;
        }

        return result;
    }

    DilationMorphology::~DilationMorphology()
    {
        delete structureElement;
    }

    ErosionMorphology::ErosionMorphology(StructureElement* p_structureElement)

    {
        structureElement = p_structureElement;
    }

    BYTE ErosionMorphology::getCurrentElementFromOperator(ImageWrapper& image, Point& currentPixels,
        std::map<int, BYTE*> linesStructureElement)
    {
        std::vector<Point> points = structureElement->getPoints();
        BYTE result = 255;

        unsigned int width = image.getWidth();
        unsigned int height = image.getHeight();

        for (unsigned int i = 0; i < points.size(); ++i)
        {
            int column = currentPixels.x + points[i].x;
            int row = currentPixels.y + points[i].y;

            if(column > 0 && column < (int)width && row > 0 && row < (int)height)
            {
                BYTE* line = linesStructureElement[points[i].y];

                if(result > line[column])
                    result = line[column];
            }
            else
                result = 0;
        }

        return result;
    }

    ErosionMorphology::~ErosionMorphology()
    {
        delete structureElement;
    }
}
