#ifndef MORPHOLOGYOPERATORS_H_INCLUDED
#define MORPHOLOGYOPERATORS_H_INCLUDED

#include "../ImageProcessor.h"
#include "StructureElement.h"
#include <map>

namespace ConnectedOperatorApp
{
    class MorphologyOperator : public ImageProcessor
    {
        protected:
            StructureElement* structureElement;

        public:
            ImageWrapper process(ImageWrapper& image);

        protected:
            std::map<int, BYTE*> getLinesFromStructureElement(ImageWrapper& image, int currentLine);
            virtual BYTE getCurrentElementFromOperator(ImageWrapper& image, Point& currentPixels, std::map<int, BYTE*> linesStructureElement) = 0;
    };

    class DilationMorphology: public MorphologyOperator
    {
        public:
            DilationMorphology(StructureElement* p_structureElement);
            ~DilationMorphology();

        protected:
            BYTE getCurrentElementFromOperator(ImageWrapper& image, Point& currentPixels, std::map<int, BYTE*> linesStructureElement);
    };

    class ErosionMorphology: public MorphologyOperator
    {
        public:
            ErosionMorphology(StructureElement* p_structureElement);
            ~ErosionMorphology();

        private:
            BYTE getCurrentElementFromOperator(ImageWrapper& image, Point& currentPixels, std::map<int, BYTE*> lineStructureElement);
    };

}

#endif // MORPHOLOGYOPERATORS_H_INCLUDED
