#ifndef IMAGEPROCESSOR_H_INCLUDED
#define IMAGEPROCESSOR_H_INCLUDED

#include "../FreeImageWrapper/ImageWrapper.h"

namespace ConnectedOperatorApp
{
    class ImageProcessor
    {
        public:
            virtual ImageWrapper process(ImageWrapper& image) = 0;
            virtual ~ImageProcessor() {}
    };
}

#endif // IMAGEPROCESSOR_H_INCLUDED
