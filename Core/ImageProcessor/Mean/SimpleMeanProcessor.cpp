#include "SimpleMeanProcessor.h"

namespace ConnectedOperatorApp
{

    SimpleMeanProcessor::SimpleMeanProcessor()
    {}

    ImageWrapper SimpleMeanProcessor::process(ImageWrapper& image)
    {
        ImageWrapper result = image.createAnEmptyCopy();
        unsigned int height = image.getHeight();
        unsigned int width = image.getWidth();
        unsigned int j;

        for (unsigned int i = 1; i < height - 1; ++i)
        {
            BYTE* l1 = image.getScanLine(i-1);
            BYTE* l2 = image.getScanLine(i);
            BYTE* l3 = image.getScanLine(i+1);
            BYTE* resultLine = result.getScanLine(i-1);

            for (j = 1; j < width - 1; ++j)
            {
                resultLine[j-1] = (l1[j-1] + l1[j] + l1[j+1] + l2[j-1] + l2[j] + l2[j+1] + l3[j-1] + l3[j] + l3[j+1]) / 9;
            }
        }

        return result;
    }
}
