#ifndef SIMPLEMEANPROCESSOR_H
#define SIMPLEMEANPROCESSOR_H

#include "../ImageProcessor.h"

namespace ConnectedOperatorApp
{
    class SimpleMeanProcessor: public ImageProcessor
    {
        public:
            SimpleMeanProcessor();
            ImageWrapper process(ImageWrapper& image);
    };
}
#endif // SIMPLEMEANPROCESSOR_H
