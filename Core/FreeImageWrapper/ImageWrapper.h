#ifndef IMAGEWRAPPER_H_INCLUDED
#define IMAGEWRAPPER_H_INCLUDED

#include <FreeImage.h>
#include <FreeImagePlus.h>
#include <iostream>

namespace ConnectedOperatorApp
{
    class ImageWrapper
    {
        protected:
            fipImage _fipImage;
            ImageWrapper(fipImage m_fipImage);

        public:
            ImageWrapper(const std::string& imageFile);
            ImageWrapper createAnEmptyCopy();

            unsigned int getWidth() const;
            unsigned int getHeight() const;
            void convertToGrayscale();
            BYTE* getScanLine(int lineIndex);
            void save(const std::string& filename);
    };
}

#endif // IMAGEWRAPPER_H_INCLUDED
