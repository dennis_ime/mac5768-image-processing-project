#include "ImageWrapper.h"

namespace ConnectedOperatorApp
{
    ImageWrapper::ImageWrapper(const std::string& imageFile)
    {
        _fipImage.load(imageFile.c_str());
    }

    ImageWrapper::ImageWrapper(fipImage m_fipImage)
    {
        _fipImage = m_fipImage;
    }

    ImageWrapper ImageWrapper::createAnEmptyCopy()
    {
        fipImage myCopy(_fipImage.getImageType(), getWidth(), getHeight(), FreeImage_GetBPP(_fipImage));
        return ImageWrapper(myCopy);
    }

    unsigned int ImageWrapper::getWidth() const
    {
        return _fipImage.getWidth();
    }

    unsigned int ImageWrapper::getHeight() const
    {
        return _fipImage.getHeight();
    }

    void ImageWrapper::convertToGrayscale()
    {
        _fipImage.convertToGrayscale();
    }

    BYTE* ImageWrapper::getScanLine(int lineIndex)
    {
        return _fipImage.getScanLine(lineIndex);
    }

    void ImageWrapper::save(const std::string& filename)
    {
        _fipImage.save(filename.c_str());
    }
}
