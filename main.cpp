#include <iostream>
#include "Core/ImageProcessor/Mean/SimpleMeanProcessor.h"
#include "Core/ImageProcessor/Morphology/MorphologyOperators.h"

using namespace std;
using namespace ConnectedOperatorApp;


int main()
{
    ImageWrapper image("bandeirantes.jpg");

    StructureElement *structureElement = new BoxStructureElement(30, 30);
    ImageProcessor *simpleMean = new ErosionMorphology(structureElement);

    image.convertToGrayscale();
    ImageWrapper result = simpleMean->process(image);

    result.save("result.jpg");
    delete simpleMean;
    return 0;
}
